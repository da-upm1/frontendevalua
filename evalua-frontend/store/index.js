import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

new Vuex.Store({
  state: () => ({
    baseURL: 'http://127.0.0.1:8000/',
    loggingIn: false,
    loginError: null,
    accessToken: null
  }),
  mutations: {
    increment (state) {
      state.counter++
    }
  },

  modules: {
    todos: {
      namespaced: true,
      state: () => ({
        list: []
      }),
      mutations: {
        add (state, { text }) {
          state.list.push({
            text,
            done: false
          })
        },
        remove (state, { todo }) {
          state.list.splice(state.list.indexOf(todo), 1)
        },
        toggle (state, { todo }) {
          todo.done = !todo.done
        },

        loginStart: state => state.loggingIn = true,
        loginStop: (state, errorMessage) => {
          state.loggingIn = false;
          state.loginError = errorMessage;
        },
        updateAccessToken: (state, accessToken) => {
          state.accessToken = accessToken;
        }
      },

    }
  },
  actions: {
    doLogin({commit}, loginData) {
      commit('loginStart');

      axios
        .post('http://127.0.0.1:8000/token/', ...loginData)
        .then(function (resp) {
          localStorage.setItem('accessToken', resp.data.access_token)
          localStorage.setItem('expires_in', resp.data.expires_in)
          commit('loginStop', null)
          commit('n', resp.data.access_token)
        })
        .catch(function (error) {
          commit('loginStop', error.response.data.error);
          commit('updateAccessToken', null);
        })
    },

    fetchAccessToken({commit}) {
      commit('updateAccessToken', localStorage.getItem('accessToken'));
    }
  }
})
